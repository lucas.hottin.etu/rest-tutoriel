package fr.ulille.iut.tva.ressource;

import java.util.ArrayList;
import java.util.List;

import fr.ulille.iut.tva.dto.InfoDetailsDto;
import fr.ulille.iut.tva.dto.InfoTauxDto;
import fr.ulille.iut.tva.service.CalculTva;
import fr.ulille.iut.tva.service.TauxTva;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

/**
 * TvaRessource
 */
@Path("tva")
public class TvaRessource {
    private CalculTva calculTva = new CalculTva();
    
    @GET
    @Path("tauxpardefaut")
    public double getValeurTauxParDefaut() {
    	return TauxTva.NORMAL.taux;
    }
    
    @GET
    @Path("valeur/{niveauTva}")
    public double getValeurTaux(@PathParam("niveauTva") String niveau) {
        try {
            return TauxTva.valueOf(niveau.toUpperCase()).taux; 
        }
        catch ( Exception ex ) {
            throw new NiveauTvaInexistantException();
        }
    }


    @GET
    @Path("{niveautva}")
    public double getMontantTotal(@PathParam("niveautva") String niveau,@QueryParam("somme") String stringSomme) {
    	 try {
    		int somme = Integer.parseInt(stringSomme);
	    	return calculTva.calculerMontant(TauxTva.valueOf(niveau.toUpperCase()), somme);
	    }
	    catch ( Exception ex ) {
	        throw new NiveauTvaInexistantException();
	    }
    }
    
    @GET
    @Path("lestaux")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<InfoTauxDto> getInfoTaux() {
      ArrayList<InfoTauxDto> result = new ArrayList<>();
      for ( TauxTva t : TauxTva.values() ) {
        result.add(new InfoTauxDto(t.name(), t.taux));
      }
      return result;
    }
    
    @GET
    @Path("details/{taux}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public InfoDetailsDto getDetails(@PathParam("taux") String niveau,@QueryParam("somme") String stringSomme){
    	InfoDetailsDto result = new InfoDetailsDto();
    	result.setMontantTotal(getMontantTotal(niveau, stringSomme));
    	result.setMontantTva(getMontantTotal(niveau, stringSomme)-Integer.parseInt(stringSomme));
    	result.setSomme(Integer.parseInt(stringSomme));
    	result.setTauxLabel(niveau.toUpperCase());
    	result.setTauxValue(TauxTva.valueOf(niveau.toUpperCase()).taux);
		return result;
    }


}
